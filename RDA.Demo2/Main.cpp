

#include <conio.h> //getch
#include <iostream> // cin/cout

using namespace std;

int main()
{
	//booleans

	/*int input;
	cout << "Enter an interger: ";
	cin >> input;

	if (input % 2 == 0) cout << input << " is even.\n";
	else cout << input << " is odd.\n";*/

	//switch-case statements

	//int i;

	//cout << "Enter a day of Christmas (1 - 12): ";
	//cin >> i;

	//switch (i)
	//{
	//case 12: cout << "Twelve Drummers Drumming\n"; // case fall-through
	//case 11: cout << "Eleven Pipers Piping\n";
	//case 10: cout << "Ten Lords a Leaping\n";
	//case  9: cout << "Nine Ladies Dancing\n";
	//case  8: cout << "Eight Maids a Milking\n";
	//case  7: cout << "Seven Swans a Swimming\n";
	//case  6: cout << "Six Geese a Laying\n";
	//case  5: cout << "Five Golden Rings\n";
	//case  4: cout << "Four Calling Birds\n";
	//case  3: cout << "Three French Hens\n";
	//case  2: cout << "Two Turtle Doves\n";
	//case  1: cout << "A Partridge in a Pear Tree\n"; break;
	//default: cout << i << "is not a valid day of Christmas.\n";
	//}

	//looping

	//another way to write infinite loop is int i = 0; for (;;) { cout <, i << "n"; }
	/*int i = 1;

	for (; i <= 10;)
	{
		cout << i << "\n";
	}*/

	int input;
	cout << "Enter an int (1-5)";
	cin >> input;
	while (input < 1 || input > 5)
	{
		cout << "really ...";
		cin >> input;
	}
	cout << "Congrats you're not an idiot.";


	_getch();
	return 0;

}